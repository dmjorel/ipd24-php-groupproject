<?php

    require_once 'vendor/autoload.php';
    require_once 'init.php';
    
    //display Home
    $app->get('/productlines', function ($request, $response, $args) {
          $productLines = DB::query("SELECT category, photo FROM products WHERE id IN
          (SELECT MIN(id) FROM products GROUP BY category)");
        return $this->view->render($response, 'productlines.html.twig',['ProductLines' => $productLines]);
    });

    $app->get('/category/{cat:[A-Za-z0-9_ -]+}', function ($request, $response, $args){
      $cat =  $args['cat'];
      if (!in_array($cat, ['pizza', 'sides', 'drinks','desserts'])) { // TODO add more
          throw new Slim\Exception\NotFoundException($request, $response); // this will cause 404
      }
      $prodList = DB::query("SELECT * FROM products WHERE category=%s", $cat);
      return $this->view->render($response, 'category.html.twig', ['prodList' => $prodList]);
  });

    $app->get('/itemdetails/{id:[0-9]+}', function ($request, $response, $args) {

         $selectedItem = DB::queryFirstRow("SELECT * FROM products WHERE id=%d", $args['id']);

       return $this->view->render($response, 'itemdetails.html.twig', ['selectedItem' =>  $selectedItem]);
    });

    $app->post('/itemdetails/{id:[0-9]+}', function ($request, $response, $args) use ($log){
        // TODO: WHAT IF NO SESSION ID?
        if($args['id']){
            $selectedProd = DB::queryFirstRow("SELECT * FROM products WHERE id=%d", $args['id']);
        }
        $errorList = [];
        if (session_id() && $selectedProd) {
            $size = $request->getParam('size');
            $quantity = $request->getParam('quantity');

            if ($quantity > $selectedProd['inStock']) {
                $errorList[] = "Out of Stock now";
                $quantity = '';
            }
            if (!isset($size)) {
                $errorList[] = "Please choose a size";
            }

            if ($errorList) {
                $log->error(sprintf("Failed to add item into cart: product id %d, uid=%d", $args['id'], $_SERVER['REMOTE_ADDR']));
                return $this->view->render($response, 'contact.html.twig', [
                    'errors' => $errorList,
                    'selectedItem' => [
                        'quantity' => $quantity
                    ]
                ]);
            } else {
                // add to cart
                $newCartItem = ['session_id' => session_id(), 'productId' => $args['id'], 'quantity' => $quantity, 'size' => $size];
                $itemInCart = DB::queryFirstRow("SELECT * FROM cartitems WHERE session_id=%s AND productId=%d AND size=%s", session_id(), $args['id'], $size);
                if($itemInCart){
                    DB::update('cartitems', ['quantity' => $itemInCart['quantity']+$quantity], "id=%d", $itemInCart['id']);
                    $_SESSION['cart'] += $quantity;
                    $log->debug(sprintf("Product id %d quantity changed in cart with session id %s, uid=%d, cart=%d", $args['id'], session_id(), $_SERVER['REMOTE_ADDR'], $_SESSION['cart']));
                }else{
                    DB::insert('cartitems', $newCartItem);
                    $_SESSION['cart'] += $quantity;
                    $log->debug(sprintf("New item added into cart: product id %d with session id %s, uid=%d, cart=%d", $args['id'], session_id(), $_SERVER['REMOTE_ADDR'], $_SESSION['cart']));
                }
                setFlashMessage("Add into cart successfully");
                return $response->withRedirect("/category/" . $selectedProd['category']);
            }
        }
    });

 
  
