<?php

// <?php include 'templates/header.php' 
// <?php include 'templates/footer.php' 

// for development we want to see all the errors, some php.ini versions disable those (e.g. MAMP)

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once 'vendor/autoload.php';

require_once 'init.php';

// Qip2mZsu0qEZ cp5003_samuraipizza

require_once 'user.php';//for user register, login
require_once 'order.php';//for order and checkout
require_once "productlines.php";

//this'/' route needs to be replaced by the menu
$app->get("/", function ($request, $response, $args) {
    return $response->write("Welcome to samurai pizza.");
   
});

// Run app
$app->run();


