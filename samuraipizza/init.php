<?php


use Slim\Http\Request;
use Slim\Http\Response;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require_once 'vendor/autoload.php';

session_start();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd24.ca") !== false) {
    // hosting on ipd24.com
    DB::$dbName = 'cp5003_samuraipizza';
    //DB::$user = 'cp5003_pizza';
    DB::$user = 'cp5003_samuraipizza';
    DB::$password = 'Qip2mZsu0qEZ';

} else {

   // DB::$dbName = 'pizza2';
   // DB::$user = 'pizza2';

    DB::$dbName = 'pizzanew';
    DB::$user = 'pizzanew';
    DB::$password = 'hV47fGeFuCSKOsQO';
    //DB::$password = hV47fGeFuCSKOsQO
    DB::$host = 'localhost';
    DB::$port = 3333;
}

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // redirect
    header("Location: /internalerror");
    die;
}


// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};


//Override the default Not Found Handler before creating App
$container['notFoundHandler'] = function ($container) {
        return function ($request, $response) use ($container) {
        $response = $response->withStatus(404);
     //   return $container['view']->render($response, 'error_notfound.html.twig');
          return $container['view']->render($response, 'admin/not_found.html.twig');
          
    };
};

// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['blogUser'] ?? null );
$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());


$passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';

// Flash messages handling
$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());

function setFlashMessage($message) {
    $_SESSION['flashMessage'] = $message;
}

// returns empty string if no message, otherwise returns string with message and clears is
function getAndClearFlashMessage() {
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}




