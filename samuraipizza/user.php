<?

require_once 'vendor/autoload.php';
require_once 'init.php';
require_once 'util.php';

use Slim\Http\Request;
use Slim\Http\Response;


// Function - Login - normal users
// STATE 1: first display
$app->get('/login', function ($request, $response, $args) {
    return $this->view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function ($request, $response, $args) use ($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    //
    $record = DB::queryFirstRow("SELECT id,name,email,password,isAdmin FROM users WHERE email=%s", $email);
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }
    }
    //

    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for email %s from %s", $email, $_SERVER['REMOTE_ADDR']));
        DB::insert('recentactivities', [
            'email' => $email, 'ip' => $_SERVER['REMOTE_ADDR'],
            'activity' => "Login failed"
        ]);
        return $this->view->render($response, 'login.html.twig', ['error' => true]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['id'], $_SERVER['REMOTE_ADDR']));
        DB::insert('recentactivities', [
            'email' => $email, 'ip' => $_SERVER['REMOTE_ADDR'],
            'activity' => "Login successful"
        ]);
        setFlashMessage("Login successful");
        if ($record['isAdmin'] == 1) {
            //admin user
            return $this->view->render($response, 'admin/admin_login_success.html.twig');
        } else {
            //normal user
            return $this->view->render($response, 'user_login_success.html.twig');
        }

        //return $this->view->render($response, 'login_success.html.twig', ['userSession' => $_SESSION['user'] ] );
    }
});





// Function - Register
// STATE 1: first display
$app->get('/register', function ($request, $response, $args) {
    return $this->view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function ($request, $response, $args) {
    $name = $request->getParam('name');
    $email = $request->getParam('email');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');
    $address = $request->getParam('address');
    $phone = $request->getParam('phone');

    //
    $errorList = array();

    //
    $result = verifyUserName($name);
    if ($result !== TRUE) {
        $errorList[] = $result;
    }
    // verify email
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList[] =  "Email does not look valid";
        $email = "";
    } else {
        // is email already in use?
        $record = DB::queryFirstRow("SELECT id FROM users WHERE email=%s", $email);
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }
    //<!--for the recaptcha v2 server site-->
    $recaptcha = $_POST['g-recaptcha-response'];
    $res = reCaptcha($recaptcha);
    if (!$res) {
        $errorList[] = 'Please check the the captcha form.';
    }
    // should return JSON with success as true
    if (!$res["success"]) {
        $errorList[] = 'Please check the the captcha form.';
    }




    $result = verifyPasswordQuailty($pass1, $pass2);
    if ($result != TRUE) {
        $errorList[] = $result;
    }
    //
    if ($errorList) { // STATE 3: errors
        return $this->view->render(
            $response,
            'register.html.twig',
            ['errorList' => $errorList, 'v' => ['name' => $name, 'email' => $email, 'address' => $address, 'phone' => $phone]]
        );
    } else { // STATE 2: all good
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
        $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT); // PASSWORD_ARGON2ID);
        DB::insert('users', [
            'name' => $name, 'email' => $email, 'password' => $pwdHashed,
            'address' => $address, 'phone' => $phone
        ]);
        return $this->view->render($response, 'register_success.html.twig');
    }
});


// Function - Logout
// STATE 1: first display
$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    DB::insert('recentactivities', [
        'email' => @$_SESSION['user']['email'], 'ip' => $_SERVER['REMOTE_ADDR'],
        'activity' => "Logout successful"
    ]);
    unset($_SESSION['user']);
    return $this->view->render($response, 'logout.html.twig', ['user' => null ]);
});


// ==============ADMIN FUNCTIONS START====================
// Function - show Admin home page
$app->get('/admin/index', function ($request, $response, $args) {
    // print_r($_SESSION['user']['isAdmin']);
    // verify if current login user is an Admin
    if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }else{
        return $this->view->render($response, 'admin/index.html.twig',['user'=>$_SESSION['user']]);
    }
});

// Function - show users
$app->get('/admin/users/list', function ($request, $response, $args) {
     // verify if current login user is an Admin
     if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    $usersList = DB::query("SELECT id,isAdmin,name,email,password FROM users");
    return $this->view->render($response, 'admin/users_list.html.twig', ['usersList' => $usersList,'user'=>$_SESSION['user']]);
});


// Function - edit users
// STATE 1: first display
$app->get('/admin/users/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
     // verify if current login user is an Admin
     if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($args['op'] == 'add' && !empty($args['id'])) || ($args['op'] == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($args['op'] == 'edit') {
        $user = DB::queryFirstRow("SELECT id,isAdmin,name,email,password,address,phone FROM users WHERE id=%d", $args['id']);
        if (!$user) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $user = [];
    }
    return $this->view->render($response, 'admin/users_addedit.html.twig', ['v' => $user, 'op' => $args['op']]);
});

// STATE 2&3: receiving submission
$app->post('/admin/users/{op:edit|add}[/{id:[0-9]+}]', function ($request, $response, $args) {
     // verify if current login user is an Admin
     if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    $op = $args['op'];
    // either op is add and id is not given OR op is edit and id must be given
    if ( ($op == 'add' && !empty($args['id'])) || ($op == 'edit' && empty($args['id'])) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    $name = $request->getParam('name');
    $isAdmin = $request->getParam('isAdmin') ?? '0';
    $email = $request->getParam('email');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');
    $address = $request->getParam('address');
    $phone = $request->getParam('phone');
    //
    $errorList = array();

    $result = verifyUserName($name);
    if ($result != TRUE) { $errorList[] = $result; }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
        $email = "";
    } else {
        // is email already in use BY ANOTHER ACCOUNT???
        if ($op == 'edit') {
            $record = DB::queryFirstRow("SELECT id,isAdmin,name,email,password,address,phone FROM users WHERE email=%s AND id != %d", $email, $args['id'] );
        } else { // add has no id yet
            $record = DB::queryFirstRow("SELECT id,isAdmin,name,email,password,address,phone FROM users WHERE email=%s", $email);
        }
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }
    // verify password always on add, and on edit/update only if it was given
    if ($op == 'add' || $pass1 != '') {
        $result = verifyPasswordQuailty($pass1, $pass2);
        if ($result != TRUE) { $errorList[] = $result; }
    }
    //
    if ($errorList) {
        return $this->view->render($response, 'admin/users_addedit.html.twig',
                [ 'errorList' => $errorList, 'v' => ['name' => $name, 'email' => $email ]  ]);
    } else {
        if ($op == 'add') {
            DB::insert('users', ['name' => $name, 'email' => $email, 'password' => $pass1, 'isAdmin' => $isAdmin, 'address' => $address, 'phone' => $phone]);
            return $this->view->render($response, 'admin/users_addedit_success.html.twig', ['op' => $op ]);
        } else {
            $data = ['name' => $name, 'email' => $email, 'isAdmin' => $isAdmin, 'address' => $address, 'phone' => $phone];
            if ($pass1 != '') { // only update the password if it was provided
                $data['password'] = $pass1;
            }
            DB::update('users', $data, "id=%d", $args['id']);
            return $this->view->render($response, 'admin/users_addedit_success.html.twig', ['op' => $op ]);
        }
    }
});

//Function - delete user
// STATE 1: first display
$app->get('/admin/users/delete/{id:[0-9]+}', function ($request, $response, $args) {
     // verify if current login user is an Admin
     if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }


    $user = DB::queryFirstRow("SELECT id,isAdmin,name,email,password FROM users WHERE id=%d", $args['id']);
    if (!$user) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/users_delete.html.twig', ['v' => $user] );
});

// STATE 1: first display
$app->post('/admin/users/delete/{id:[0-9]+}', function ($request, $response, $args) {
    // // verify if current login user is an Admin
    if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    DB::delete('users', "id=%d", $args['id']);
    return $this->view->render($response, 'admin/users_delete_success.html.twig' );
});



// Function - show users
$app->get('/admin/recentactivities', function ($request, $response, $args) {
    // verify if current login user is an Admin
    if (!isset($_SESSION['user']['isAdmin']) || $_SESSION['user']['isAdmin']!=1) {
       $response = $response->withStatus(404);
       return $this->view->render($response, 'admin/not_found.html.twig');
   }
   $usersList = DB::query("SELECT id,email,ip,creationTS,activity FROM recentactivities ORDER BY creationTS DESC");
   return $this->view->render($response, 'admin/recentactivities.html.twig', ['usersList' => $usersList,'user'=>$_SESSION['user']]);
});