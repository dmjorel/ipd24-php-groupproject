<?

require_once 'vendor/autoload.php';
require_once 'init.php';
require_once 'util.php';

use Slim\Http\Request;
use Slim\Http\Response;


// Function - Login - normal users
// STATE 1: first display
$app->get('/login', function ($request, $response, $args) {
    return $this->view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function ($request, $response, $args) use ($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    //
    $record = DB::queryFirstRow("SELECT id,name,email,password,isAdmin FROM users WHERE email=%s", $email);
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }

    }
    //
    
    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for email %s from %s", $email, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'login.html.twig', [ 'error' => true ]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['id'], $_SERVER['REMOTE_ADDR']));       
        setFlashMessage("Login successful");
        if ($record['isAdmin']===1) {
            //admin user
            return $this->view->render($response, 'admin_login_success.html.twig');
        }else{
            //normal user
            return $this->view->render($response, 'user_login_success.html.twig');
        }
        
        //return $this->view->render($response, 'login_success.html.twig', ['userSession' => $_SESSION['user'] ] );
    }
});





// Function - Register
// STATE 1: first display
$app->get('/register', function ($request, $response, $args) {
    return $this->view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function ($request, $response, $args) {
    $name = $request->getParam('name');
    $email = $request->getParam('email');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');
    $address = $request->getParam('address');
    $phone = $request->getParam('phone');
    
    //
    $errorList = array();
    
    //
    $result = verifyUserName($name);
    if ($result !== TRUE) { $errorList[] = $result; }
       // verify email
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList [] =  "Email does not look valid" ;
        $email = "";
    } else {
        // is email already in use?
        $record = DB::queryFirstRow("SELECT id FROM users WHERE email=%s", $email);
        if ($record) {
            array_push($errorList, "This email is already registered");
            $email = "";
        }
    }
    //<!--for the recaptcha v2 server site-->
    $recaptcha = $_POST['g-recaptcha-response'];
    $res = reCaptcha($recaptcha);
      if(!$res){
        $errorList [] = 'Please check the the captcha form.';
      }
        // should return JSON with success as true
        if(!$res["success"]) {
            $errorList [] = 'Please check the the captcha form.';      
        }



  
    $result = verifyPasswordQuailty($pass1, $pass2);
    if ($result != TRUE) { $errorList[] = $result; }
    //
    if ($errorList) { // STATE 3: errors
        return $this->view->render($response, 'register.html.twig',
                [ 'errorList' => $errorList, 'v' => ['name' => $name, 'email' => $email ,'address' => $address,'phone' => $phone]  ]);
    } else { // STATE 2: all good
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
        $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT); // PASSWORD_ARGON2ID);
        DB::insert('users', ['name' => $name, 'email' => $email, 'password' => $pwdHashed,
                    'address'=>$address,'phone'=>$phone]);
        return $this->view->render($response, 'register_success.html.twig');
    }
});


//functions - login - admin users
