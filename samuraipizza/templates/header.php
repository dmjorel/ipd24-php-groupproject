<head>
    <title>Samurai Pizza</title>
    <!-- for css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
</head>
<body class="grey lighten-4"> <!-- end body tag ends in footer -->
    <nav class="white z-depth-0">
        <div class="container">
            <a href="#" class="brand-logo brand-text">Samurai Pizza</a>
            <ul id="nav-mobile">
                <li><a href="#" class="btn brand z-depth-0">Make an Order</a></li>
            </ul>
        </div>
    </nav>

