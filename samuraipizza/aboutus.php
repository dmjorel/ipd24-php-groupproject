<?

require_once 'vendor/autoload.php';
require_once 'init.php';
require_once 'util.php';

use Slim\Http\Request;
use Slim\Http\Response;

// Function - about us
// STATE 1: first display

$app->get('/aboutus', function ($request, $response, $args) {
    return $this->view->render($response, 'aboutus.html.twig');
});

$app->run();
