<?php
// index.php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>PHP Web lesson Tutorial</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <body>
        <br/>
        <div class="container">
            <br/>
            <h3 align="center">Simple PHP MySQL Shopping Cart with Stripe Integration</h3><br/>
            <br/>

            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Menu</span>
                            <span class="glyphicon glyphicon-menu-hamburger"></span>
                        </button>
                        <a class="navbar-brand" href="/">Webslesson</a>
                    </div>

                    <div is="navbar-cart" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a id="cart-popover" class="btn" data-placement="bottom" title="Shopping Cart">
                                <span class="glyphicon glyphicon-shoppping-cart"></span>
                                <span class="badge"></span>
                                <span class="totla_price">$ 0.00</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="popover_content_wrapper" style="display: none">
                <span id="card-details"></span>
                <div align="right">
                    <a href="order_process.php" class="btn btn-primary" id="check_out_cart">
                        <span class="glyphicon glyphicon-shoppping-cart"></span> Check out
                    </a>
                    <a href="#" class="btn btn-default" id="clear_cart">
                        <span class="glyphicon glyphicon-trash"></span> Clear
                    </a>
                </div>
            </div>
            <?php
                if(isset($_SESSION["success_message"]))
                {
                    echo '<div class="alert alert-success">'.$_SESSION["success_message"].'</div>';
                    unset($_SESSION["success_message"]);
                }
            ?>
            <div id="display_item" class="row">
            
            </div>
        </div>
    </body>
</html>

<script>
$(document).ready(function(){

    load_product();

    load_cart_data();

    function load_product()
    {
        $.ajax({
            url:"fetch_item.php",
            method:"POST",
            success: function(data){
                $('#display_item').html(data);
            }
        })
    }

    function load_cart_data()
    {
        $.ajax({
            url:"fetch_cart.php",
            method:"POST",
            dataType:"json",
            success:function(data)
            {
                $('#card_details').html(data.card_details);
                $('.total_price').text(data.total_price);
                $('.badge').text(data.total_item);
            }
        })
    }

    $('#cart-popover').popover({
        html : true,
        container : 'body',
        content:function(){
            return $('#popover_content_wrapper').html();
        }
    });

    $(document).on('click','.add_to_cart', function(){
        var product_id = $(this).attr('id');
        var product_name = $('#name' + product_id + '').val();
        var product_price = $('#price' + product_id + '').val();
        var product_quantity = $('#quantity' + product_id).val();
        var action = 'add';
        if(product_quantity > 0)
        {
            $.ajax({
                url:"action.php",
                method:"POST",
                data:{
                    product_id:product_id, 
                    product_name:product_name, 
                    product_price:product_price,
                    product_quantity:product_quantity,
                    action:action
                    },
                success:function(data)
                {
                    load_cart_data();
                    alert("Item has been added into cart");
                }
            })
        }
        else
        {
            aler("Please Enter Number Of Quantity");
        }
    });

    $(document).on('click', '.delete', function(){
        var product_id = $(this).attr('id');
        var action = 'remove';
        if(confirm("Are you sure you want to remove this product?"))
        {
            $.ajax({
                url:"action.php",
                method:"POST",
                data:{product_id:productid, action:action},
                success:function(data)
                {
                    load_cart_data();
                    $('#cart-popover').popover('hide');
                    alert("Item has been removed from Cart");
                }
            })
        }
        else
        {
            return false;
        }
    });

    $(document).on('click', '#clear_cart', function(){
        var action = 'empty';
        $.ajax({
            url:"action.php",
            method:"POST",
            data:{action:action},
            success:function()
            {
                load_cart_data();
                $('#cart-popover').popover('hide');
                alert("Your cart has been cleared");
            }
        })
    })

});

</script>