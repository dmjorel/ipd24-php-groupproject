<?

require_once 'vendor/autoload.php';
require_once 'init.php';
require_once 'util.php';

use Slim\Http\Request;
use Slim\Http\Response;

// Function - order
// STATE 1: first display

$app->get('/order', function ($request, $response, $args) {
    /*
    if (!isset($_SESSION['currUser'])) { 
        $response = $response->withStatus(403);
        return $this->view->render($response, 'login.html.twig');
    }
    */
    
    $id = $request -> getParam('#c.id'); // current customer

    $priceTotal = $request -> getParam('#price'); // prices total of all selected products

    $status = 'paidAndPlaced';


    // validation
    $errorList = [];
    // adding order
    $valuesList = ['custId' => $id, 'priceTotal' => $priceTotal, 'status' => $status];
    DB::insert('orders', ['custId' => $id, 'priceTotal' => $priceTotal, 'status' => $status]);
    return $this->view->render($response, 'order.html.twig', ['errorList' => $errorList, 'v' => $valuesList]);
    //return $response->write("welcome to checkout page");
});



